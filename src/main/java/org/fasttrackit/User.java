package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

// user -> (user/ password
public class User {
    private final Credentials credentials;
    private final Account defaultAccount;
    private final List<Account> accounts;
    private final String name;
    private final String surName;
    private  String email;
    private  String phone;

    public User(String loginId, String password, String name, String surName  ) {
       this.credentials = new Credentials(loginId,password);
       this.defaultAccount = new Account("RO55BTRLRONCTR01100i1010",  "RON");
       this.name = name;
       this.surName = surName;
       this.email = "";
       this.phone = "";
       this.accounts = new ArrayList<>();
    }
    public void registerAccount(Account newAccount){
        this.accounts.add(newAccount);
    }
    public boolean isAuthenticated(Credentials credentials) {
        return this.credentials.equals(credentials);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account getDefaultAccount() {
        return defaultAccount;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
