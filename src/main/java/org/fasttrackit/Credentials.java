package org.fasttrackit;

import java.util.Objects;

public class Credentials {
    private final String loginId;
    private  final String password;


    public Credentials(String loginId, String password) {
        this.loginId = loginId;
        this.password = password;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credentials that = (Credentials) o;
        return Objects.equals(loginId, that.loginId) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loginId, password);
    }
}

