package org.fasttrackit;

import static org.fasttrackit.View.*;
import static org.fasttrackit.View.GBP;

public class AccountFactory { static Account createAccount(String userOption) {
    Account account = null;
    switch (userOption){
        case RON: {
            account = new Account("RO55BTRLRONCTR01100i1011",  "RON");
            break;
        }
        case EUR: {
            account = new Account("RO55BTRLEURCTR01100i1011",  "EUR");
            break;
        }
        case USD:{
            account = new Account("RO55BTRLUSDCTR01100i1011",  "USD");
            break;
        }
        case GBP: {
            account = new Account("RO55BTRLGBPCTR01100i1011", "GBP");
            break;
        }
        default:
            System.out.println("Sorry, unknown option. ");
    }
    return account;
}

}
