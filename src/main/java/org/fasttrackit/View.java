package org.fasttrackit;

import java.util.Scanner;

public class View {
    //showNavigationMenu
    public static final String CHECK_ACCOUNT_BALANCE = "2";
    public static final String VIEW_ACCOUNT_DETAILS = "1";
    public static final String CREATE_NEW_ACCOUNT = "3";
    public static final String EXIT = "6";

    //ShowSupportedCurrency
    public static final String RON = "1";
    public static final String EUR = "2";
    public static final String USD = "3";
    public static final String GBP = "4";

    //showWelcomingScreen
    public static final String REGISTER_NEW_ACCOUNT = "1";
    public static final String LOGIN_WITH_EXISTING_ACCOUNT = "2";


     static void showWelcomingScreen() {
        System.out.println("Welcome to the new FastTrack Bank application");
        System.out.println();
        System.out.println("What operation would you like to do?");
        System.out.println("1. Register new account");
        System.out.println("2. Login with existing account");
        System.out.print("Your selection:  ");
    }

     static void showContactSupport() {
        System.out.println("You heve reached max login attempts.");
        System.out.println("Please contact support for more details.");
        System.out.println("Contact us via email: sopport@fasttrakbank.org or via phone: 0800 800 0800");
    }

     static void showNavigationMenu() {
        System.out.println("------------------------");
        System.out.println("1. View account details");
        System.out.println("2. Check account balance");
        System.out.println("3. Create new account");
        System.out.println("4. Exchange rates");
        System.out.println("5. Transfer between accounts");
        System.out.println("6. Exit");
        System.out.println("What would you like to do ?");
    }
    static void ShowSupportedCurrency() {
        System.out.println("Supported currencies ");
        System.out.println("1. RON");
        System.out.println("2. EUR");
        System.out.println("3. USD");
        System.out.println("4. GBP");
        System.out.print("Select the currency for the new account: ");
    }

    static void showAccountDetails(Account account) {
        System.out.println("IBAN: "+ account.getIban());
        System.out.println("Balance " + account.getBalance());
        System.out.println("Account Currency: "+ account.getCurrency());
    }

    static void askUserForRegistrationDetails(Scanner keyboard) {
        System.out.print("Please enter user:  ");
        String user = keyboard.nextLine();
        System.out.print("Please enter password: ");
        String password = keyboard.nextLine();


        System.out.print("Please enter name:  ");
        String name = keyboard.nextLine();
        System.out.print("Please enter surname: ");
        String surname = keyboard.nextLine();

        System.out.print("Please enter email:  ");
        String email = keyboard.nextLine();
        System.out.print("Please enter Phone nr:  ");
        String phoneNr = keyboard.nextLine();
    }
}
